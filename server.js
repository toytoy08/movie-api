'use strict'

const express = require("express");
const bodyParser = require("body-parser")
const mongoose = require("mongoose")
mongoose.connect("mongodb://localhost:27017/movie_db")

const db=mongoose.connection
db.on("error", (err) => {
    console.error(err)
})
db.once("open", () => {
    console.log("db connected")
    const Movie = require('./movie')
    Movie.create({
        name:'Fake',
        image_url:'Fake URL',
        overview:'Fake overviwe'
    },(err,movie)=>{
        console.log(err)
        console.log(movie)
    })
})

const app = express()

const port = 8000

let counter = 2

const movies = [
    {
        id: 1,
        title: 'fake title 1',
        image_url: 'fake image url',
        overview: 'fake overview'
    }, {
        id: 2,
        title: 'fake title 2',
        image_url: 'fake image url',
        overview: 'fake overview'
    }
]

const favorites = []

app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())

// app.use(logMw1)
// app.use(logMw2)

app.get("/movies", (reg, res) => {
    console.log(reg.user)
    const results = {
        results: movies
    }
    res.json(results)
})

app.get("/favorites", (reg, res) => {
    const results = {
        results: favorites
    }
    res.json(results)
})

app.post("/favorites", (reg, res) => {

    const fav = {
        id: ++counter,
        title: reg.body.title
    }
    favorites.push(fav)
    res.json(fav)
})

app.delete("/favorites/:id", (reg, res) => {
    const id = parseInt(reg.params.id, 10)
    const index = favorites.findIndex(fav => fav.id === id)
    if (index === -1) {
        res.sendStatus(404)
    }
    favorites.splice(index, 1)
    res.sendStatus(200)
})

app.listen(port, () =>
    console.log(`started at ${port}`))